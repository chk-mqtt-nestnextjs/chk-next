FROM node:15.2.0-buster-slim

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
COPY . .

RUN npm run build
EXPOSE 7000

CMD ["npm","run","prod-node"]
