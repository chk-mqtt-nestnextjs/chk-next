import React from "react";
import Link from "next/link";
import styles from "./CardItem.module.scss";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
  Image,
  Badge,
  GridItem,
} from "@chakra-ui/react";

const CardItem = ({ src, text, label, user }) => {
  return (
    // <>
    //   <li className={styles.cards_item}>
    //     <Link href={"/"}>
    //       <div className={styles.cards_item_link}>
    //         <figure
    //           className={styles.cards_item_pic_wrap}
    //           data-category={label}
    //         >
    //           <img
    //             className={styles.cards_item_img}
    //             alt="TravelImage"
    //             src={src}
    //           />
    //         </figure>
    //         <div className={styles.cards_item_info}>
    //           {/* <h5 className={styles.cards_item_text}>{text}</h5> */}
    //           <div className={styles.cards_item_text} dangerouslySetInnerHTML={{ __html: text }} />

    //         </div>
    //       </div>
    //     </Link>
    //   </li>
    // </>

    <GridItem colSpan={1} style={{position: "relative"}}>
      <Box borderRadius="lg" overflow="hidden" boxShadow="xl">
        <Image
          src={src}
          alt={label}
          height={"350px"}
          width={"100%"}
          _hover={{ transform: "scale(1.1)", transition: "all 0.2s linear" }}
        />

        <Box p="6">
          <Badge
            colorScheme="purple"
            variant="outline"
            style={{
              position: "absolute",
              top: "350px",
            }}
          >
            {user}
          </Badge>
          <Box d="flex" alignItems="baseline">
            <Box style={{ width: "100%" }}>
              <Accordion allowMultiple>
                <AccordionItem style={{ border: "0px" }}>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        {label}
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    <div dangerouslySetInnerHTML={{ __html: text }} />
                  </AccordionPanel>
                </AccordionItem>
              </Accordion>
            </Box>
          </Box>
        </Box>
      </Box>
    </GridItem>
  );
};

export default CardItem;
