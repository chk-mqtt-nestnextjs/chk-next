import {
  Input,
  FormControl,
  FormLabel,
  InputGroup,
  InputLeftElement,
  FormErrorMessage,
  Code,
  Icon,
  Image,
  Box,
} from "@chakra-ui/react";
import { FiFile } from "react-icons/fi";
import { useRef, useState, useEffect } from "react";
import { restfulService } from "../../../services/restful.service";

const FileUpload = ({ isLogin, setFormValue, formValue }) => {
  const inputRef = useRef();
  const [image, setImage] = useState(null);
  const [fileName, setFileName] = useState(null);
  // const [createObjectURL, setCreateObjectURL] = useState(null);

  const uploadToServer = async (event) => {
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];
      setImage(i);
      //   setCreateObjectURL(URL.createObjectURL(i));
      const form = new FormData();
      form.append("file", i);
      form.append("userId", isLogin["id"]);
      await restfulService
        .create("/api/files/image/upload", form)
        .then((res) => res.data)
        .then((res) => {
          console.log(res.name);
          setFileName(res.name);
          setFormValue((prev) => ({
            ...prev,
            filesId: res.id,
          }));
        })
        .catch((err) => console.log(err));
    }
  };

  useEffect(async () => {
    const { filesId } = formValue;
    if (filesId) {
      await restfulService
        .getById("/api/files", filesId)
        .then((res) => res.data)
        .then((res) => {
          setFileName(res[0].name);
        })
        .catch((err) => console.log(err));
    }
  }, [formValue]);

  return (
    <>
      <FormControl>
        <InputGroup>
          <InputLeftElement
            pointerEvents="none"
            children={<Icon as={FiFile} />}
          />
          <input
            type="file"
            name={"file"}
            accept="image/*"
            style={{ display: "none" }}
            ref={inputRef}
            onChange={uploadToServer}
          ></input>
          <Input
            placeholder={"Your file ..."}
            value={image?.name || ""}
            onClick={() => inputRef.current.click()}
            readOnly
          />
        </InputGroup>
        {fileName ? (
          <Box>
            <Image src={`/api/files/image/${fileName}`} alt="Segun Adebayo" />
          </Box>
        ) : null}
      </FormControl>
    </>
  );
};

export default FileUpload;
