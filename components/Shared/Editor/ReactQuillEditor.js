import React from "react";
import styles from "./ReactQuillEditor.module.scss";
import ReactQuill from "react-quill";

const ReactQuillEditor = ({ setFormValue , formValue }) => {
  // const [value, setValue] = useState("");

  // useEffect(() => {
  //   setFormValue((prev) => ({
  //     ...prev,
  //     body: value,
  //   }));
  // }, [value]);

  return (
    <ReactQuill
      theme="snow"
      value={formValue.body}
      onChange={(e) =>
        setFormValue((prev) => ({
          ...prev,
          body: e,
        }))
      }
    />
  );
};

export default ReactQuillEditor;
