// import styled from "@emotion/styled";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

const theme = createMuiTheme({
  palette: {
    common: {
      main: "#87B185"
    },
    primary: {
      main: "#007CFF",
    },
    secondary: { 
      main: "#e91e63",
      contrastText: "#fff" 
    },
  },
});

const Layout = (props) => {
  return (
    <ThemeProvider theme={theme}>
        <div className="page-layout">{props.children}</div>
    </ThemeProvider>
  );
};

// const LayoutnStyled = styled.div`
  //   body {
  //     margin: 0;
  //     padding: 0;
  //     font-size: 18px;
  //     font-weight: 400;
  //     line-height: 1.8;
  //     color: #333;
  //     font-family: sans-serif;
  //   }
// `;

export default Layout;
