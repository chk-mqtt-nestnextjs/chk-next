import React from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./Card.module.scss";
import { Flex, Spacer, Box, Grid, GridItem } from "@chakra-ui/react";

const Card = ({ posts }) => {
  // const mockdatas = [
  //   { id: 1, src: "/assets/images/img-1.jpg", text: "TEST1", label: "TEST1" },
  //   { id: 2, src: "/assets/images/img-2.jpg", text: "TEST1", label: "TEST2" },
  //   { id:3, src: "/assets/images/img-3.jpg", text: "TEST1", label: "TEST3" },
  //   { id:4, src: "/assets/images/img-4.jpg", text: "TEST1", label: "TEST4" },
  //   { id:5, src: "/assets/images/img-5.jpg", text: "TEST1", label: "TEST5" },
  //   { id:6, src: "/assets/images/img-6.jpg", text: "TEST1", label: "TEST6" },
  //   { id:7, src: "/assets/images/img-7.jpg", text: "TEST1", label: "TEST7" },
  //   { id:8, src: "/assets/images/img-8.jpg", text: "TEST1", label: "TEST8" },
  //   { id:9, src: "/assets/images/img-9.jpg", text: "TEST1", label: "TEST9" },
  // ];

  return (
    <div className={styles.cards}>
      <h1 className={styles.card_title}>Peek&Yee</h1>
      <div className={styles.cards_container}>
        {/* <div className={styles.cards_wrapper}>
          <ul className={styles.cards_items}>
            {posts
              ? posts.map((res, i) => (
                  <CardItem
                    src={`/api/files/image/${res?.files?.name}`}
                    text={res.body}
                    label={res.title}
                    key={res.id}
                  />
                ))
              : ""}
          </ul> 
       </div> */}
        <Grid
          templateColumns={{ md: "repeat(3, 1fr)", sm: "repeat(1, 1fr)" }}
          gap={6}
        >
            {posts
              ? posts.map((res, i) => (
                  <CardItem
                    src={`/api/files/image/${res?.files?.name}`}
                    text={res.body}
                    label={res.title}
                    key={res.id}
                    user={res.user.email}
                  />
                ))
              : ""}
        </Grid>
      </div>
    </div>
  );
};

export default Card;
