import React, { useState } from "react";
import styles from "./Doughnut.module.scss";
import { Doughnut as DoughnutChartjs } from "react-chartjs-2";

const Doughnut = () => {
  const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  const [chartData, setChartData] = useState({
    labels: ["TFK", "Mullard", "Siemens","Philco","Valvo","RCA"],
    datasets: [
      {
        data: [
          getRandomInt(8, 200),
          getRandomInt(5, 150),
          getRandomInt(50, 250),
          getRandomInt(23, 250),
          getRandomInt(15, 250),
          getRandomInt(33, 250),
        ],
        backgroundColor: ["#CCC", "#36A2EB", "#FFCE56"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
      },
    ],
  });

  return (
    <>
    <div className="header">
        <h1 className="title">{'ECC82&ECC83市場佔有率'}</h1>
      </div>
      <DoughnutChartjs
        style={{paddingBottom: "30px"}}
        data={chartData}
        options={{
          padding: "50px",
          responsive: true,
          maintainAspectRatio: false,
          defaultFontSize: "14px",

          height: "400",
          legend: {
            display: false,
          },
        }}
      />
    </>
  );
};

export default Doughnut;
