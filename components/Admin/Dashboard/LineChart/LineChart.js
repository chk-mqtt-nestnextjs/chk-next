import React, { useEffect, useState } from "react";
import styles from "./LineChart.module.scss";
import { Line } from "react-chartjs-2";
import * as moment from "moment";

const LineChart = () => {
  const [month, setMonth] = useState(null);
  const data = {
    labels: [],
    datasets: [
      {
        label: "ecc82價錢",
        data: [],
        fill: false,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgba(255, 99, 132, 0.2)",
      },
      {
        label: "ecc83價錢",
        data: [],
        fill: false,
        backgroundColor: "rgb(44, 165, 226)",
        borderColor: "rgba(19, 140, 200, 0.2)",
      },
    ],
  };

  const options = {
    padding: "10px",
    responsive: true,
    maintainAspectRatio: false,
    defaultFontSize: "14px",
    height: "200",
    legend: {
      display: false,
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  useEffect(() => {
    if (!month) {
      const currentDays = moment().daysInMonth();
      let index = 1;
      [...Array(currentDays)].map((x) => {
        data.labels.push(moment(new Date()).format('MMM DD YYYY') +" "+ index);
        data.datasets[0].data.push(Math.floor(Math.random() * 100 + 1));
        data.datasets[1].data.push(Math.floor(Math.random() * 100 + 1));
        index++;
      });
    }
  }, []);

  return (
    <>
      {/* <div className="header">
        <h1 className="title">
          {moment(new Date()).format("ddd MMM DD YYYY")}
        </h1>
      </div> */}
      <Line data={data} options={options} />
    </>
  );
};

export default LineChart;
