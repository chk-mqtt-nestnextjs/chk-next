import React from "react";
import styles from "./Dashboard.module.scss";
// import { Doughnut } from "react-chartjs-2";
import { Grid, GridItem, Box } from "@chakra-ui/react";
import LineChart from "./LineChart/LineChart";
import Doughnut from "./Doughnut/LineChart/Doughnut";

const Dashboard = () => {
 
  return (
    <div className={styles.dashboardContainer}>
      <Grid
        templateRows={{ md: "repeat(2, 1fr)", sm: "repeat(4, 1fr)" }}
        templateColumns={{ md: "repeat(5, 1fr)", sm: "repeat(1, 1fr)" }}
        gap={4}
      >
        <GridItem
          rowSpan={{ md: 2, sm: 0 }}
          colSpan={{ md: 1, sm: 5 }}
          // bg="whiteAlpha.900"
          // h={'1500px'}
        >
          {/* <div className={styles.doughnut_canvas_container}>
           
          </div> */}
          <Box
            bg="whiteAlpha.900"
            // maxH="300px"
            w="100%"
            p={4}
            color="black"
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
          ></Box>
        </GridItem>
        <GridItem colSpan={{ md: 2, sm: 5 }}>
          {/* <div className={styles.doughnut_canvas_container}> */}

          <Box
            bg="whiteAlpha.900"
            maxH="300px"
            w="100%"
            p={4}
            color="black"
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
          >
            <Doughnut />
          </Box>
          {/* </div> */}
        </GridItem>
        <GridItem colSpan={{ md: 2, sm: 5 }}>
          {/* <div className={styles.doughnut_canvas_container}>
            <Doughnut />
          </div> */}
          <Box
            bg="whiteAlpha.900"
            maxH="300px"
            w="100%"
            p={4}
            color="black"
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
          >
            <Doughnut />
          </Box>
        </GridItem>
        <GridItem colSpan={{ md: 4, sm: 5 }}>
          {/* <div className={styles.line_container}>
            <LineChart />
          </div> */}
          <Box
            bg="whiteAlpha.900"
            w="100%"
            p={4}
            color="black"
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
          >
            <LineChart />
          </Box>
        </GridItem>
      </Grid>
    </div>
  );
};

export default Dashboard;
