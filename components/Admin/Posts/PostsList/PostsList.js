import React, { useEffect, useState, useContext } from "react";
import styles from "./PostsList.module.scss";
import { Table, Thead, Tbody, Tr, Th, Td, Box } from "@chakra-ui/react";
import PostsFormModal from "../PostsFormModal/PostsFormModal";
import { restfulService } from "../../../../services/restful.service";
import { Context as ContextProvider } from "../../../../context/ContextProvider";

const PostsList = () => {
  const { storeReturnMessage } = useContext(ContextProvider);
  const [postLists, setPostLists] = useState(null);
  const [isUpdate, setIsUpdate] = useState(false);
  //todo debug
  useEffect(async () => {
    if (!postLists) {
      await restfulService
        .getAll("/api/posts")
        .then((res) => res.data)
        .then((res) => setPostLists(res))
        .catch((err) => {
          storeReturnMessage({
            type: "error",
            message: err,
          });
        });
    }
  }, [postLists]);

  return (
    <Box
      bg="white"
      w="100%"
      p={4}
      color="black"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
    >
      <PostsFormModal setPostLists={setPostLists} type="create" />
      <br />
      <br />
      <Table size="sm">
        <Thead>
          <Tr>
            <Th>Post Id</Th>
            <Th>Title</Th>
            <Th>Create by</Th>
            <Th>Action</Th>
          </Tr>
        </Thead>
        <Tbody>
          {postLists ? (
            postLists.map((listValue, index) => {
              return (
                <Tr key={index}>
                  <Td>{listValue.id}</Td>
                  <Td>{listValue.title}</Td>
                  <Td>{listValue?.user?.email}</Td>
                  <Td>
                    <PostsFormModal
                      setPostLists={setPostLists}
                      postId={listValue.id}
                      type="edit"
                    />
                  </Td>
                </Tr>
              );
            })
          ) : (
            <Tr>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td></Td>
            </Tr>
          )}
        </Tbody>
      </Table>
    </Box>
  );
};

export default PostsList;
