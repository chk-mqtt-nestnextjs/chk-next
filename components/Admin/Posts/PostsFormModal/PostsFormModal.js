import React, { useState, useContext } from "react";
import styles from "./PostsFormModal.module.scss";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Box,
  useDisclosure,
  FormControl,
  FormLabel,
  Input,
  Button,
} from "@chakra-ui/react";
import ReactQuillEditor from "../../../Shared/Editor/ReactQuillEditor";
import FileUpload from "../../../Shared/FileUpload/FileUpload";
import { Context as AuthProvider } from "../../../../context/AuthProvider";
import { Context as ContextProvider } from "../../../../context/ContextProvider";
import { restfulService } from "../../../../services/restful.service";

const PostsFormModal = ({ setPostLists, postId, type }) => {
  const { storeReturnMessage } = useContext(ContextProvider);
  const { isLogin } = useContext(AuthProvider);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = React.useRef();
  const finalRef = React.useRef();

  const [formValue, setFormValue] = useState({
    title: "",
    body: "",
    filesId: null,
  });

  const onEditPost = async (postId) => {
    console.log(postId);
    setFormValue({
      title: "",
      body: "",
      filesId: null,
    });
    await restfulService
      .getById(`/api/posts`, postId)
      .then((res) => res.data)
      .then((res) => {
        console.log(res);
        setFormValue({
          title: res[0].title,
          body: res[0].body,
          filesId: res[0].filesId,
        });
      })
      .catch((err) => {
        storeReturnMessage({
          type: "error",
          message: err,
        });
      });

    onOpen();
  };

  const onSubmit = async () => {
    if (
      formValue.title.length > 0 &&
      formValue.body.length > 0 &&
      isLogin["id"]
    ) {
      await restfulService
        .create("/api/posts", {
          title: formValue.title,
          body: formValue.body,
          filesId: formValue.filesId,
          usersId: isLogin["id"],
        })
        .then(() => {
          setFormValue({
            title: "",
            body: "",
            filesId: null,
          });

          storeReturnMessage({
            type: "success",
            message: "Post has been created",
          });

          setPostLists(null);
          onClose();
        })
        .catch((err) => {
          console.log(err);
          storeReturnMessage({
            type: "error",
            message: err,
          });
        });
    }
  };

  const onUpdate = async() => {
    console.log(formValue);
    if (
      formValue.title.length > 0 &&
      formValue.body.length > 0 &&
      isLogin["id"]
    ) {
      await restfulService
        .update("/api/posts", postId, {
          title: formValue.title,
          body: formValue.body,
          filesId: formValue.filesId,
          usersId: isLogin["id"],
        })
        .then(() => {
          setFormValue({
            title: "",
            body: "",
            filesId: null,
          });

          storeReturnMessage({
            type: "success",
            message: "Post has been updated",
          });

          setPostLists(null);
          onClose();
        })
        .catch((err) => {
          console.log(err);
          storeReturnMessage({
            type: "error",
            message: err,
          });
        });
    }
  };

  return (
    <>
      {/* <Button onClick={onOpen}>Open Modal</Button> */}
      {type === "create" ? (
        <Box
          float="right"
          as="button"
          borderRadius="md"
          bg="green"
          // bgGradient="radial(gray.300,yellow.400,pink.200)"
          color="white"
          px={8}
          h={8}
          onClick={onOpen}
        >
          新增
        </Box>
      ) : (
        <Box
          as="button"
          borderRadius="md"
          bg="blue"
          // bgGradient="radial(gray.300,yellow.400,pink.200)"
          color="white"
          px={4}
          h={8}
          onClick={(e) => onEditPost(postId)}
        >
          Edit
        </Box>
      )}

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create Post</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Title</FormLabel>
              <Input
                ref={initialRef}
                placeholder="Title"
                value={formValue.title}
                onChange={(e) =>
                  setFormValue((prev) => ({
                    ...prev,
                    title: e.target.value.trim(),
                  }))
                }
              />
            </FormControl>
            <FormControl mt={4}>
              <FormLabel>Image</FormLabel>
              <FileUpload
                isLogin={isLogin}
                setFormValue={setFormValue}
                formValue={formValue}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Body</FormLabel>
              <ReactQuillEditor
                setFormValue={setFormValue}
                formValue={formValue}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            {postId ? (
              <Button colorScheme="blue" mr={3} onClick={onUpdate}>
                Update
              </Button>
            ) : (
              <Button colorScheme="blue" mr={3} onClick={onSubmit}>
                Save
              </Button>
            )}

            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default PostsFormModal;
