import React, { useState } from "react";
import styles from "./Sidebar.module.scss";
// import { HomeIcon, PeopleIcon, ChatIcon } from '@material-ui/icons';
import HomeIcon from "@material-ui/icons/Home";
import PeopleIcon from "@material-ui/icons/People";
import ChatIcon from "@material-ui/icons/Chat";
import { useContext } from "react";
import { AdminContext } from "../../../context/AdminRouteProvider";
import {
  Flex,
  Icon,
  Link,
} from '@chakra-ui/react';
import {
  FiFile,
  FiUsers,
  FiHome,
  FiPhoneCall
} from 'react-icons/fi';

const Sidebar = () => {
  const { storeCurrentPage } = useContext(AdminContext);
  const siderbarlist = [
    {
      name: "Home",
      // icon: <HomeIcon />,
      icon: FiHome,
      href: "dashboard",
    },
    {
      name: "Member",
      // icon: <PeopleIcon />,
      icon: FiUsers,
      href: "member",
    },
    {
      name: "Posts",
      // icon: <PeopleIcon />,
      icon: FiFile,
      href: "posts",
    },
    {
      name: "Chat",
      // icon: <ChatIcon />,
      icon: FiPhoneCall,
      href: "chats",
    },
  ];

  const handleClick = (val) => {
    storeCurrentPage(val);
  };

  return (
    // <div className={styles.sidebar}>
    //   <ul className={styles.sidebarlist} style={{paddingLeft: '0px'}}>
    //     {siderbarlist.map((val, index) => (
    //       <li
    //         className={styles.row}
    //         key={index}
    //         onClick={()=> handleClick(val.link)}
    //       >
    //         {" "}
    //         <div className={styles.icon}> {val.icon}</div>
    //         <div className={styles.title}>{val.title}</div>
    //       </li>
    //     ))}
    //   </ul>
    // </div>
    <>
      {siderbarlist.map((link) => (
        <NavItem key={link.name} icon={link.icon}  onClick={()=> handleClick(link.href)}>
          {link.name}
        </NavItem>
      ))}
    </>
  );
};

const NavItem = ({ icon, children, ...rest }) => {
  return (
    <Link style={{ textDecoration: 'none' }}>
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: 'cyan.400',
          color: 'white',
        }}
        {...rest}>
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white',
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Link>
  );
};

export default Sidebar;
