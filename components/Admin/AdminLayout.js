import React, { useEffect } from "react";
import styles from "./AdminLayout.module.scss";
import Sidebar from "./Sidebar/Sidebar";
import Dashboard from "./Dashboard/Dashboard";
import MemberList from "./MemberList/MemberList";
import { useContext } from "react";
import { Context } from "../../context/AuthProvider";
import { useRouter } from "next/router";
import { AdminContext } from "../../context/AdminRouteProvider";
// import { Container, Row, Col } from 'react-bootstrap';
import {
  IconButton,
  Box,
  CloseButton,
  Flex,
  HStack,
  VStack,
  useColorModeValue,
  Drawer,
  DrawerContent,
  Text,
  useDisclosure,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { FiMenu } from "react-icons/fi";
import { IconType } from "react-icons";
import Link from "next/link";
import PostsList from "./Posts/PostsList/PostsList";
import Chats from "./Chats/Chats";

// const LinkItems = [
//   { name: 'Home', icon: FiHome },
//   { name: 'Trending', icon: FiTrendingUp },
//   { name: 'Explore', icon: FiCompass },
//   { name: 'Favourites', icon: FiStar },
//   { name: 'Settings', icon: FiSettings },
// ];

const AdminLayout = ({ children }) => {
  const router = useRouter();

  const { isLogin, isLogout } = useContext(Context);

  const { currentPage } = useContext(AdminContext);

  const onPageChange = (page) => {
    switch (page) {
      case "dashboard":
        return <Dashboard className={styles.element_item} />;
      case "member":
        return <MemberList className={styles.element_item} />;
      case "posts":
        return <PostsList className={styles.element_item} />;
      case "chats":
        return <Chats className={styles.element_item} />;
    }
  };

  // useEffect(async () => {
  //   const callback = () => {
  //     console.log("isLogin 2");
  //     console.log(isLogin);
  //     setTimeout(() => {
  //       if (
  //         !isLogin["isLogin"] ||
  //         !isLogin["isActive"] ||
  //         isLogin["role"] !== "admin"
  //       ) {
  //         console.log("isLogin 3");
  //         router.push("/");
  //       }
  //     }, 1000);
  //   };
  //   await callback();
  // }, []);
  // return (
  //   <div className={styles.adminlayout_container}>
  //     <Sidebar className={styles.sidebar_item} />
  //     {onPageChange(currentPage)}
  //   </div>
  // );

  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box minH="100vh" bg={useColorModeValue("gray.100", "gray.900")}>
      <SidebarContent
        onClose={() => onClose}
        display={{ base: "none", md: "block" }}
      />
      <Drawer
        autoFocus={false}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav
        onOpen={onOpen}
        isLogin={isLogin}
        isLogout={isLogout}
        router={router}
      />
      <Box ml={{ base: 0, md: 60 }} p="4">
        {onPageChange(currentPage)}
      </Box>
    </Box>
  );
};

const SidebarContent = ({ onClose, ...rest }) => {
  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue("white", "gray.900")}
      borderRight="1px"
      borderRightColor={useColorModeValue("gray.200", "gray.700")}
      w={{ base: "full", md: 60 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
          <Link href="/"> Peek&Yee </Link>
        </Text>
        <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
      </Flex>
      <Sidebar />
      {/* {LinkItems.map((link) => (
        <NavItem key={link.name} icon={link.icon}>
          {link.name}
        </NavItem>
      ))} */}
    </Box>
  );
};

const MobileNav = ({ onOpen, isLogin, isLogout, router }) => {
  const onLogout = () => {
    localStorage.removeItem("jwt");
    isLogout().then(() => {
      router.push("/");
      localStorage.removeItem("jwt");
    });
  };

  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="20"
      alignItems="center"
      bg={useColorModeValue("white", "gray.900")}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue("gray.200", "gray.700")}
      justifyContent={{ base: "space-between", md: "flex-end" }}
    >
      <IconButton
        display={{ base: "flex", md: "none" }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <Text
        display={{ base: "flex", md: "none" }}
        fontSize="2xl"
        fontFamily="monospace"
        fontWeight="bold"
      >
        Logo
      </Text>

      <HStack spacing={{ base: "0", md: "6" }}>
        <Flex alignItems={"center"}>
          <Menu>
            <MenuButton
              py={2}
              transition="all 0.3s"
              _focus={{ boxShadow: "none" }}
            >
              <HStack>
                <VStack
                  display={{ base: "none", md: "flex" }}
                  alignItems="flex-start"
                  spacing="1px"
                  ml="2"
                >
                  <Text fontSize="sm">{isLogin?.email}</Text>
                  <Text fontSize="xs" color="gray.600">
                    {isLogin?.role}
                  </Text>
                </VStack>
              </HStack>
            </MenuButton>
            <MenuList
              bg={useColorModeValue("white", "gray.900")}
              borderColor={useColorModeValue("gray.200", "gray.700")}
            >
              <MenuItem onClick={onLogout}>Sign out</MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </HStack>
    </Flex>
  );
};

export default AdminLayout;
