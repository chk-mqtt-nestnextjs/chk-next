import React from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDocker } from "@fortawesome/free-brands-svg-icons";
import {
  faFacebook,
  faInstagram,
  faWhatsapp,
} from "@fortawesome/free-brands-svg-icons";
import styles from "./Footer.module.scss";
import { useRouter } from "next/router";

const Footer = () => {
  const { asPath } = useRouter();
  return (
    <>
      {asPath === "/admin" ? (
        <></>
      ) : (
        <div className={styles.footer_container}>
          <section className={styles.social_media}>
            <div className={styles.social_media_wrap}>
              <div className={styles.footer_logo}>
                <Link href="/">
                  <div className={styles.social_logo}>
                    DOCKER
                    <FontAwesomeIcon icon={faDocker} />
                  </div>
                </Link>
              </div>
              <small className={styles.website_rights}>
                CALVIN_CHAN © 2020
              </small>
              <div className={styles.social_icons}>
                <Link href="/" target="_blank" aria-label="Facebook">
                  <div className={styles.social_icon_link}>
                    <FontAwesomeIcon icon={faFacebook} />
                  </div>
                </Link>
                <Link href="/" target="_blank" aria-label="Instagram">
                  <div className={styles.social_icon_link}>
                    <FontAwesomeIcon icon={faInstagram} />
                  </div>
                </Link>
                <Link href="/" target="_blank" aria-label="Whatsapp">
                  <div className={styles.social_icon_link}>
                    <FontAwesomeIcon icon={faWhatsapp} />
                  </div>
                </Link>
              </div>
            </div>
          </section>
        </div>
      )}
    </>
  );
};

export default Footer;
