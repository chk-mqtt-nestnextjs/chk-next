import React, { useState, useEffect } from "react";

import { withTranslation } from "next-i18next";
import styles from "./SignUpAndSignIn.module.scss";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

import { restfulService } from "../../../services/restful.service";
import { useContext } from "react";
import { Context as  AuthProvider} from "../../../context/AuthProvider";
import { Context as  ContextProvider } from "../../../context/ContextProvider";
import { Button as ChakraButton } from "@chakra-ui/react";

const SignUpAndSignIn = ({ t, type }) => {
  const { storeReturnMessage } = useContext(ContextProvider);
  const { storeLoginStatus } = useContext(AuthProvider);
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = async () => {
    if (form.email.length > 0 && form.password.length > 0) {
      await restfulService
        .create("/api/auth/register", form)
        .then(() => {
          setForm({
            email: "",
            password: "",
          });
          handleClose();
        })
        .catch((err) => console.log(err));
    }
  };

  const onLogin = async () => {
    if (form.email.length > 0 && form.password.length > 0) {
      await restfulService
        .create("/api/auth/login", form)
        .then((res) => res.data)
        .then((res) => {
          const { id, email, isActive, role, jwt } = res;
          if (jwt) {
            localStorage.setItem("jwt", jwt);
            storeLoginStatus({ id, email, isActive, role });
          }
          handleClose();
        })
        .catch((err) => {
          console.log(err);
          storeReturnMessage({
            type: 'error',
            message: 'User account and password not correct'
          })
        });
    }
  };

  // useEffect(() => {}, []);

  return (
    <>
      {/* <div className={styles.nav_links} onClick={handleClickOpen}>
        {type == "signup" ? t("sign-up") : t("sign-in")}
      </div> */}
      <ChakraButton
        as={"a"}
        fontSize={"sm"}
        fontWeight={200}
        variant={"link"}
        onClick={handleClickOpen}
      >
        {type == "signup" ? t("sign-up") : t("sign-in")}
      </ChakraButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {type == "signup" ? t("sign-up-account") : t("sign-in-account")}
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="email"
            type="email"
            fullWidth
            value={form.email}
            onChange={(e) =>
              setForm((prev) => ({
                ...prev,
                email: e.target.value.trim(),
              }))
            }
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="password"
            type="password"
            value={form.password}
            onChange={(e) =>
              setForm((prev) => ({
                ...prev,
                password: e.target.value.trim(),
              }))
            }
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          {type == "signup" ? (
            <Button onClick={onSubmit} color="primary">
              Register
            </Button>
          ) : (
            <Button onClick={onLogin} color="primary">
              Login
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

export default withTranslation()(SignUpAndSignIn);
