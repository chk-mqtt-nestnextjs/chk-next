import React, {useEffect, useState} from "react";
import { useRouter } from "next/router";
// import Link from "next/link";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import {
//   faHeadphones,
//   faTimes,
//   faBars,
// } from "@fortawesome/free-solid-svg-icons";
// import LanguageSwitcher from "../LanguageSwitcher/LanguageSwitcher";
import { withTranslation } from "next-i18next";
import styles from "./Header.module.scss";
import SignUpAndSignIn from "./SignUpAndSignIn/SignUpAndSignIn";
import { useContext } from "react";
import { Context } from "../../context/AuthProvider";

import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Collapse,
  Link,
  Popover,
  PopoverTrigger,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  Center,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";

const Header = ({ t }) => {
  const { asPath } = useRouter();
  const { isLogin, isLogout } = useContext(Context);
  // const [click, setClick] = useState(false);
  // const closeMobileMenu = () => setClick(false);

  const onLogout = () => {
    localStorage.removeItem("jwt");
    isLogout();
  };

  // const handleClick = () => {
  //   setClick(!click);
  // };
  const { isOpen, onToggle } = useDisclosure();

  const NAV_ITEMS = [
    {
      label: t("home"),
      href: "/",
    },
    {
      label: t("aboutus"),
      href: "/aboutus",
    },
    {
      label: t("sponsor-me"),
      href: "/",
    },
  ];

  if (isLogin?.isLogin && isLogin?.isActive) {
    NAV_ITEMS.push({
      label: t("admin"),
      href: "/admin",
    });
  }

  // const [chakraCss, setChakraCss] = useState({
  //   flexBg: '',
  //   flexColor: '',
  //   flexBorderColor: '',
  //   textAlign: '',
  //   textColor: ''
  // })

  // useEffect(() => {
  //   setChakraCss({
  //     flexBg: useColorModeValue("white", "gray.800"),
  //     flexColor: useColorModeValue("gray.600", "white"),
  //     flexBorderColor: useColorModeValue("gray.200", "gray.900"),
  //     textAlign: useBreakpointValue({ base: "center", md: "left" }),
  //     textColor: useColorModeValue("gray.800", "white")
  //   })
  // }, [asPath])

  return (
    <>
      {asPath === "/admin" ? (
        <></>
      ) : (
        <Box>
          <Flex
            bg={"white"}
            color={'gray.600'}
            minH={"60px"}
            py={{ base: 2 }}
            px={{ base: 4 }}
            borderBottom={1}
            borderStyle={"solid"}
            borderColor={'gray.200'}
            align={"center"}
          >
            <Flex
              flex={{ base: 1, md: "auto" }}
              ml={{ base: -2 }}
              display={{ base: "flex", md: "none" }}
            >
              <IconButton
                onClick={onToggle}
                icon={
                  isOpen ? (
                    <CloseIcon w={3} h={3} />
                  ) : (
                    <HamburgerIcon w={5} h={5} />
                  )
                }
                variant={"ghost"}
                aria-label={"Toggle Navigation"}
              />
            </Flex>
            <Flex flex={{ base: 1 }} justify={{ base: "center", md: "start" }}>
              <Center>
                <Text
                  textAlign={{ base: "center", md: "left" }}
                  fontFamily={"heading"}
                  color={"gray.800"}
                  fontSize={"28px"}
                  fontWeight={"bold"}
                >
                  {"Peek&Yee"}
                </Text>
              </Center>
              <Flex display={{ base: "none", md: "flex" }} ml={10}>
                <DesktopNav NAV_ITEMS={NAV_ITEMS} />
              </Flex>
            </Flex>
            {isLogin?.isLogin && isLogin?.isActive ? (
              <Stack
                flex={{ base: 1, md: 0 }}
                justify={"flex-end"}
                direction={"row"}
                spacing={1}
              >
                <Button
                  as={"a"}
                  fontSize={"sm"}
                  fontWeight={400}
                  variant={"link"}
                  onClick={onLogout}
                >
                  {t("logout")}
                </Button>
              </Stack>
            ) : (
              <Stack
                flex={{ base: 1, md: 1 }}
                justify={"flex-end"}
                direction={"row"}
                spacing={1}
              >
                <SignUpAndSignIn type={"signup"} />
                <SignUpAndSignIn type={"signin"} />
              </Stack>
            )}
          </Flex>

          <Collapse in={isOpen} animateOpacity>
            <MobileNav NAV_ITEMS={NAV_ITEMS} />
          </Collapse>
        </Box>
      )}
    </>

    // <div className={styles.navbar}>
    //   <div className={styles.navbar_container}>
    //     <Link href="/">
    //       <div className={styles.navbar_logo}>
    //         <FontAwesomeIcon
    //           className={styles.header_icon}
    //           icon={faHeadphones}
    //         />
    //         <label className={styles.title}>{`DashBoard`}</label>
    //       </div>
    //     </Link>
    //   </div>
    //   <div className={styles.menu_icon} onClick={handleClick}>
    //     <FontAwesomeIcon icon={click ? faTimes : faBars} />
    //   </div>

    //   <ul className={click ? styles.nav_menu_active : styles.nav_menu}>
    //     <li className={styles.nav_item} key={"home"}>
    //       <Link href="/" onClick={closeMobileMenu}>
    //         <div className={styles.nav_links}>
    //           <label>{t("home")}</label>
    //         </div>
    //       </Link>
    //     </li>
    //     <li className={styles.nav_item} key={"aboutUs"}>
    //       <Link href="/aboutus" onClick={closeMobileMenu}>
    //         <div className={styles.nav_links}>{t("aboutus")}</div>
    //       </Link>
    //     </li>
    //     <li className={styles.nav_item} key={"sponsorMe"}>
    //       <Link href="/" onClick={closeMobileMenu}>
    //         <div className={styles.nav_links}>{t("sponsor-me")}</div>
    //       </Link>
    //     </li>
    //     {!isLogin?.isLogin && !isLogin?.isActive ? (
    //       <>
    //         <li className={styles.nav_item} key={"signup"}>
    //           <SignUpAndSignIn type={"signup"} />
    //         </li>
    //         <li className={styles.nav_item} key={"signin"}>
    //           <SignUpAndSignIn type={"signin"} />
    //         </li>
    //       </>
    //     ) : null}

    //     {isLogin?.isLogin && isLogin?.isActive ? (
    //       <>
    //         <li className={styles.nav_item} key={"admin"}>
    //           <Link href="/admin" onClick={closeMobileMenu}>
    //             <div className={styles.nav_links}>{t("admin")}</div>
    //           </Link>
    //         </li>
    //         <li className={styles.nav_item} key={"logout"}>
    //           <div className={styles.nav_links} onClick={onLogout}>
    //             {t("logout")}
    //           </div>
    //         </li>
    //       </>
    //     ) : null}

    //     <li className={styles.nav_item} key={"languageSwitche"}>
    //       <div className={styles.nav_language_switcher}>
    //         <LanguageSwitcher />
    //       </div>
    //     </li>
    //   </ul>
    // </div>
  );
};

const DesktopNav = (data) => {
  const linkColor = useColorModeValue("gray.600", "gray.200");
  const linkHoverColor = useColorModeValue("gray.800", "white");
  const popoverContentBgColor = useColorModeValue("white", "gray.800");

  return (
    <Stack direction={"row"} spacing={4}>
      {data.NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Center>
                <Link
                  p={2}
                  href={navItem.href ?? "#"}
                  fontSize={"sm"}
                  fontWeight={500}
                  color={linkColor}
                  _hover={{
                    textDecoration: "none",
                    color: linkHoverColor,
                  }}
                >
                  {navItem.label}
                </Link>
              </Center>
            </PopoverTrigger>

            {/* {navItem.children && (
              <PopoverContent
                border={0}
                boxShadow={"xl"}
                bg={popoverContentBgColor}
                p={4}
                rounded={"xl"}
                minW={"sm"}
              >
                <Stack>
                  {navItem.children.map((child) => (
                    <DesktopSubNav key={child.label} {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )} */}
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};

// const DesktopSubNav = ({ label, href, subLabel }) => {
//   return (
//     <Link
//       href={href}
//       role={"group"}
//       display={"block"}
//       p={2}
//       rounded={"md"}
//       _hover={{ bg: useColorModeValue("pink.50", "gray.900") }}
//     >
//       <Stack direction={"row"} align={"center"}>
//         <Box>
//           <Text
//             transition={"all .3s ease"}
//             _groupHover={{ color: "pink.400" }}
//             fontWeight={500}
//           >
//             {label}
//           </Text>
//           <Text fontSize={"sm"}>{subLabel}</Text>
//         </Box>
//         <Flex
//           transition={"all .3s ease"}
//           transform={"translateX(-10px)"}
//           opacity={0}
//           _groupHover={{ opacity: "100%", transform: "translateX(0)" }}
//           justify={"flex-end"}
//           align={"center"}
//           flex={1}
//         >
//           <Icon color={"pink.400"} w={5} h={5} as={ChevronRightIcon} />
//         </Flex>
//       </Stack>
//     </Link>
//   );
// };

const MobileNav = (data) => {
  return (
    <Stack
      bg={useColorModeValue("white", "gray.800")}
      p={4}
      display={{ md: "none" }}
    >
      {data.NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
};

const MobileNavItem = ({ label, children, href }) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        as={Link}
        href={href ?? "#"}
        justify={"space-between"}
        align={"center"}
        _hover={{
          textDecoration: "none",
        }}
      >
        <Text
          fontWeight={600}
          color={useColorModeValue("gray.600", "gray.200")}
        >
          {label}
        </Text>
        {/* {children && (
          <Icon
            as={ChevronDownIcon}
            transition={"all .25s ease-in-out"}
            transform={isOpen ? "rotate(180deg)" : ""}
            w={6}
            h={6}
          />
        )} */}
      </Flex>

      {/* <Collapse in={isOpen} animateOpacity style={{ marginTop: "0!important" }}>
        <Stack
          mt={2}
          pl={4}
          borderLeft={1}
          borderStyle={"solid"}
          borderColor={useColorModeValue("gray.200", "gray.700")}
          align={"start"}
        >
          {children &&
            children.map((child) => (
              <Link key={child.label} py={2} href={child.href}>
                {child.label}
              </Link>
            ))}
        </Stack>
      </Collapse> */}
    </Stack>
  );
};
export default withTranslation()(Header);
