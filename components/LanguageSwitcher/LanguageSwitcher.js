import { i18n } from "next-i18next";
import { useRouter } from "next/router";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const LanguageSwitcher = () => {
  const classes = useStyles();
  const router = useRouter();
  const onChangeLanguage = (lang) => {
    if (i18n.changeLanguage(lang)) {
      router.replace(router.asPath, router.asPath, { locale: lang });
    }
  };
  return (
    <>
      <div className={classes.root}>
        <ButtonGroup
          variant="text"
          color="primary"
          aria-label="text primary button group"
        >
          <Button onClick={() => onChangeLanguage("tc")} className="button">
            TC
          </Button>

          <Button onClick={() => onChangeLanguage("en")} className="button">
            EN
          </Button>
        </ButtonGroup>
      </div>
    </>
  );
};

export default LanguageSwitcher;
