const path = require("path");

module.exports = {
    i18n: {
      defaultLocale: 'tc',
      locales: ['tc', 'en'],
      localeDetection: false 
    },
    defaultLanguage: "tc",
    otherLanguages: ["en"],
    localePath: path.resolve('./public/static/locales')
  }