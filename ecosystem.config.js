module.exports = {
    apps : [
        {
            name: "next_frontend",
            mode: "cluster",
            script: "./server.js",
            watch: true,
            instances  : 2,
            env_development: {
                "PORT": 8888,
                "NODE_ENV": "development"
            },
            env_production: {
                "PORT": 8888,
                "NODE_ENV": "production",
            }
        }
    ]
}