const next = require("next");
const { createProxyMiddleware } = require("http-proxy-middleware");
// process.env.NODE_ENV = "production";
const dev = process.env.NODE_ENV !== "production";
const app = next({
  dev,
  dir: ".",
  quiet: false,
});

const apiPaths = {
  "/api": {
    target: "http://127.0.0.1:3000/",
    pathRewrite: {
      '^/api': "",
    },
    changeOrigin: true,
  },
};


const apiProdPaths = {
  "/api": {
    target: "http://127.0.0.1/",
    pathRewrite: {
      '^/api': "/api",
    },
    changeOrigin: true,
  },
};

const handle = app.getRequestHandler();

const cors = require("cors");
const express = require("express");
const http = require("http");
// const https = require('https');

const ports = {
  http: 7000,
  https: 7000,
};

// const httpsOptions = {
//     key: fs.readFileSync('./cloudflare/lepurecomhk.key', 'utf8'),
//     cert: fs.readFileSync('./cloudflare/lepurecomhk.pem', 'utf8')
// };

app.prepare().then(async () => {
  const server = express();

  server.use(cors({ credentials: true, origin: true }));
  if (process.env.NODE_ENV !== "production") {
    server.use("/api", createProxyMiddleware(apiPaths["/api"]));
    http.createServer(server).listen(ports.http);
  } else {
    console.log(`https`);
    // server.use("/api", createProxyMiddleware(apiProdPaths["/api"]));
    http.createServer(server).listen(ports.http);
    // https.createServer(httpsOptions, server).listen(ports.https);
  }

  // static resources should not be redirected by i18n middleware to same network trip
  // highly recommend add any extension of static resources here, though it would still work if you don't
  server.all(/\.(js|json|png|jpg|ico)$/i, (req, res) => {
    return handle(req, res);
  });

  server.get("*", (req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader(
      "Access-Control-Allow-Headers",
      "X-Requested-With,content-type"
    );
    handle(req, res);
  });
  

  // Error middleware
  server.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send("Something broke!");
  });

  process.on("unhandledRejection", (error, promise) => {
    console.log("Unhandled Rejection at:", promise, "reason:", error);
    // Application specific logging, throwing an error, or other logic here
  });

});
