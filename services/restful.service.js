import axios from "axios";

export const restfulService = {
  getAll,
  getById,
  create,
  update,
  delete: _delete,
};

const isServer = typeof window === "undefined";

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);
axios.interceptors.request.use(
  (config) => {
    console.log('isServer', isServer);
    if (!isServer) {
      config.headers["Authorization"] =
        localStorage.getItem("jwt") != null || localStorage.getItem("jwt") == ""
          ? localStorage.getItem("jwt")
          : null;
      config.timeout = 1000;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export function getAll(path) {
  // console.log(isServer);
  // path = isServer ? "http://localhost:3000" + path.replace("/api", "") : path;
  return axios.get(path);
}
export function getById(path, id) {
  return axios.get(`${path}/${id}`);
}

export function create(path, params) {
  return axios.post(path, params);
}

export function update(path, id, params) {
  return axios.put(`${path}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
export function _delete(path, id) {
  return axios.delete(`${path}/${id}`);
}
