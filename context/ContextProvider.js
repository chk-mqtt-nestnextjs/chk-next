import React, { createContext, useState } from "react";

export const Context = createContext(null);

const ContextProvider = (props) => {

  const [returnMessage, setReturnMessage] = useState({
    type: null,
    message: null
  });

  const storeReturnMessage = ({type, message}) => {
    // const typeList = ["info", "warning", "success", "error"];
    setReturnMessage({
      type: type,
      message: message
    });
  };

  const [loader, setLoader] = useState(false);
  const storeLoader = (data) => {
    setLoader(data);
  };

  return (
    <Context.Provider value={{ returnMessage, storeReturnMessage, loader, storeLoader }}>
      {props.children}
    </Context.Provider>
  );
};

export default ContextProvider;