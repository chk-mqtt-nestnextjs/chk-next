import React, { createContext, useState, useEffect } from "react";
import { restfulService } from "../services/restful.service";

export const Context = createContext(null);

const AuthProvider = (props) => {
  // const [isLoaded, setIsLoaded] = useState(false);
  const [isLogin, setIsLogin] = useState({
    isLogin: false,
    isActive: false,
    email: null,
    role: null,
    id: 0,
  });

  const storeLoginStatus = (data) => {
    console.log(data);
    setIsLogin({
      isLogin: true,
      role: data.role,
      email: data.email,
      isActive: data.isActive,
      id: data.id,
    });
  };

  const isLogout = () => {
    return new Promise((resolve, reject) => {
      try {
        setIsLogin({
          isLogin: false,
          isActive: false,
          email: null,
          role: null,
          id: null,
        });
        resolve(true);
      } catch {
        (e) => {
          reject(e);
        };
      }
    });
  };

  useEffect(async() => {
    let isCancelled = false;
    const getUserInfo = async () => {
      console.log("getUserInfo 1");
      if (!isLogin["isLogin"]) {
        if (!isCancelled) {
          await restfulService
            .getAll(`/api/auth/check`)
            .then((res) => res.data)
            .then((res) => {
              if (res) {
                setIsLogin({
                  isLogin: true,
                  role: res.role,
                  email: res.email,
                  isActive: res.isActive,
                  id: res.id,
                });
                return () => {
                  isCancelled = true;
                };
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
        // }
      }
    };
    getUserInfo();
    return () => {
      isCancelled = true;
    };
  }, [isLogin]);

  return (
    <Context.Provider value={{ isLogin, storeLoginStatus, isLogout }}>
      {props.children}
    </Context.Provider>
  );
};

export default AuthProvider;
