import React, { createContext, useState } from "react";

export const AdminContext = createContext(null);

const AdminRouteProvider = (props) => {
  const [currentPage, setCurrentPage] = useState("dashboard");


  const storeCurrentPage = (page) => {
    console.log('yee:' +page);
    setCurrentPage(page);
  };

  return (
    <AdminContext.Provider value={{ currentPage, storeCurrentPage }}>
      {props.children}
    </AdminContext.Provider>
  );
};

export default AdminRouteProvider;