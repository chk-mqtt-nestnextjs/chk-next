import style from "../styles/globals.scss";
import React from "react";
import SignupForm from "../components/SignupForm/SignupForm";
import { withTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

const Signup = () => {
  return (
    <div className={style.layout_container}>
      <SignupForm />
    </div>
  );
};

export async function getServerSideProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export default withTranslation("common")(Signup);
