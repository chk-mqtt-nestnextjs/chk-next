import { useEffect } from "react";
// import styled from "@emotion/styled";
import Router from "next/router";

const Error = () => {
  useEffect(() => {
    Router.push("/");
  });
  return <></>;
};

// const ErrorStyled = styled.div`
//   * {
//     box-sizing: border-box;
//     margin: 0;
//     padding: 0;
//     font-family: "PT Sans", sans-serif;
//   }
// `;

export default Error;
