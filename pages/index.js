import style from "../styles/globals.scss";
import React, {useContext, useEffect} from "react";
import Card from "../components/Card/Card";
import { withTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { restfulService } from "../services/restful.service";
import { Context } from "../context/AuthProvider";

const Home = (props) => {
  const { posts } = props;

  return (
    <div className={style.layout_container}>
      <Card posts={posts} />
    </div>
  );
};

export async function getServerSideProps({ locale }) {
  const posts = await restfulService
    .getAll(
      `${
        process.env.SERVERSIDE_ENDPOINT
          ? process.env.SERVERSIDE_ENDPOINT + "/api/posts"
          : "http://127.0.0.1:3000/posts"
      }`
    )
    .then((res) => res.data);

  return {
    props: {
      posts,
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export default withTranslation("common")(Home);
