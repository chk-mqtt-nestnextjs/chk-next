import style from "../styles/globals.scss";
import React, { useEffect, useContext } from "react";
import { withTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import dynamic from "next/dynamic";
import { Context } from "../context/AuthProvider";
import { useRouter } from "next/router";
import { restfulService } from "../services/restful.service";
// import SocketIOClient from "socket.io-client";

const AdminComponentWithCustomLoading = dynamic(
  () => import("../components/Admin/AdminLayout"),
  {
    loading: () => <></>,
    ssr: false,
  }
);

const Admin = () => {
  const router = useRouter();
  const { isLogin, storeLoginStatus} = useContext(Context);
  // const [connected, setConnected] = useState(false);
  // const [socket, setSocket] = useState(null);
  // useEffect(() => {
  //   // connect to socket server
  //   if (!connected) {
  //     const socket = SocketIOClient.connect("http://127.0.0.1:3000");
  //     // log socket connection
  //     socket.on("connect", () => {
  //       console.log("SOCKET CONNECTED!", socket.id);
  //       setSocket(socket);
  //       setConnected(true);
  //       socket.emit('msgToServer');
  //       socket.emit('event');
  //     });

  //     // socket disconnet onUnmount if exists
  //     if (socket)
  //       return () => {
  //         console.log("SOCKET disconnect ", socket.id);
  //         socket.disconnect();
  //       };
  //   }
  // }, []);

  useEffect(() => {
    let isCancelled = false;
    const getUserInfo = async () => {
      if (!isLogin["isLogin"]) {
        if (!isCancelled) {
          await restfulService
            .getAll(`/api/auth/check`)
            .then((res) => res.data)
            .then((res) => {
              if (res) {
                storeLoginStatus({
                  isLogin: true,
                  role: res.role,
                  email: res.email,
                  isActive: res.isActive,
                  id: res.id,
                });
                if (!res["isActive"] || res["role"] !== "admin") {
                  router.push("/");
                }
                return () => {
                  isCancelled = true;
                };
              } else {
                router.push("/");
              }
            })
            .catch((err) => {
              console.log(err);
              router.push("/");
            });
        }
      }
    };
    getUserInfo();
    return () => {
      isCancelled = true;
    };
  }, [isLogin]);

  return (
    <div className={style.admin_layout_container}>
      <AdminComponentWithCustomLoading />
    </div>
  );
};

export async function getServerSideProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export default withTranslation("common")(Admin);
