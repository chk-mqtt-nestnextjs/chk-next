import "../styles/globals.scss";
import React, { useEffect } from "react";
import Head from "next/head";
// import PropTypes from "prop-types";
import ContextProvider from "../context/ContextProvider";
import AuthProvider from "../context/AuthProvider";
import AdminRouteProvider from "../context/AdminRouteProvider";
import Layout from "../components/Layout";
import CssBaseline from "@material-ui/core/CssBaseline";
import FullPageLoader from "../utils/FullPageLoader";
import { appWithTranslation } from "next-i18next";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import { ChakraProvider } from "@chakra-ui/react"
import { restfulService } from "../services/restful.service";
import Toast from "../utils/Toast";

const MyApp = ({ Component, pageProps }) => {
  // const isMobile = Boolean(
  //   userAgent.match(
  //     /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
  //   )
  // );

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    if ("serviceWorker" in navigator) {
      window.addEventListener("load", function () {
        navigator.serviceWorker.register("/sw.js").then(
          function (registration) {
            console.log(
              "Service Worker registration successful with scope: ",
              registration.scope
            );
          },
          function (err) {
            console.log("Service Worker registration failed: ", err);
          }
        );
      });
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>NextJS</title>
      </Head>
      <ChakraProvider>
      <Layout>
        <CssBaseline />
        <AuthProvider>
          <ContextProvider>
            <AdminRouteProvider>
              <FullPageLoader />
              <Toast />
              <Header />
              <Component
                {...pageProps}
                // userAgent={userAgent}
                // deviceType={isMobile ? "mobile" : "desktop"}
              />
              <Footer />
            </AdminRouteProvider>
          </ContextProvider>
        </AuthProvider>
      </Layout>
      </ChakraProvider>
    </React.Fragment>
  );
};

MyApp.getInitialProps = async ({ Component, ctx, req }) => {
  // const cookie = ctx.req.cookies.jwt ? ctx.req.cookies.jwt : null;
  let pageProps = {};
  let isLogin = false;
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  const userAgent =
    typeof window === "undefined"
      ? ctx.req.headers["user-agent"]
      : window.navigator.userAgent;

  return {
    userAgent: userAgent,
    namespacesRequired: ["common"],
    ...pageProps,
  };
};

export default appWithTranslation(MyApp);
