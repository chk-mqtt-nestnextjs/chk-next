import React, { useContext, useEffect } from "react";
import { Context } from "../context/ContextProvider";
import { useToast } from "@chakra-ui/react";

const Toast = () => {
  const { returnMessage } = useContext(Context);
  const toast = useToast();
  let element = null;

  useEffect(() => {
    if (returnMessage.type) {
      element = toast({
        title: "",
        description: returnMessage.message,
        status: returnMessage.type,
        isClosable: true,
        position: "top",
      });
    }
  }, [returnMessage]);

  return <div style={{ display: "none" }}>{element}</div>;
};

export default Toast;
