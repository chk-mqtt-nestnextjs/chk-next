const path = require("path");

require('dotenv').config({
  path: path.resolve(__dirname, `./environments/.env${process.env.NODE_ENV ? '.' + process.env.NODE_ENV : ''}`)
});
// const withPWA = require("next-pwa");
const withSass = require("@zeit/next-sass");
const { i18n } = require("./next-i18next.config");
const localeSubpaths = {
  tc: 'tc',
  en: 'en'
};

module.exports = 
  withSass({
    cssModules: true,
    // pwa: {
    //   dest: "public",
    //   scope: "/",
    // },
    env: {},
    publicRuntimeConfig: {localeSubpaths},
    webpack: (config) => {
      config.resolve.alias["components"] = path.join(__dirname, "components");
      config.resolve.alias["public"] = path.join(__dirname, "public");
      return config;
    },
    sassOptions: {
      includePaths: [path.join(__dirname, "styles")],
      prependData: `@import "~@styles/_variables.scss";`,
    },
    i18n
  });
